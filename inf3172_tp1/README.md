# INF3172 Tp1
Présentation du logiciel
------------------------

> C'est un logiciel qui gère un fichier de comptes bancaires et
> les conflits d'écriture potentiels.
> Le dossier contient: un fichier main.c; un fichier cpt.txt; 
> un fichier Makefile et un fichier Readme.

Installation du logiciel
------------------------

> Le logiciel doit être téléchargé et la compilation se fait sur ligne de 
> commande avec le compilateur GNU GCC. Pour le compiler vous devez lancer le 
> logiciel make dans le repertoire contenant les sources et le fichier cpt.txt  
> qui executera le fichier Makefile pour faire le build.
> Un fichier executable "tp1" sera créé. 


Utilisation du logiciel
-----------------------

> Le logiciel doit être lancé à la console en executant "tp1" ou par la commande "make start" 
> qui compile et execute le logiciel.

> Le fichier des comptes bancaires contient la liste des comptes bancaires
> Chaque compte est équivalent à un ligne de 17 caractères dont le format est 
> "aaaa bbbbbb cccc" plus un retour de ligne où aaaa est le code du compte,
> bbbbbb est un nom et cccc est le solde du compte.

> Exemple d'exécution du logiciel : ./tp1 ou make start