/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * Julien Lafontaine LAFJ22059206
 * TP1 – Hiver 2016 
 * Partage de fichiers.
 * Fichier : main.c
 * 
 * Il s’agit de réaliser un gestionnaire de comptes bancaires à l'aide de la
 * manipulation de fichiers notamment ceux qui concernent le partage de 
 * fichiers entre plusieurs processus (lseek, lockf).
 * L'utilisation détaillée du logiciel est dans le fichier Readme.
 *
 * Resultat tous les tests passent.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //fork 
#include <sys/wait.h> //wait
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>

int ptr_cheque;

// déclaration des fonctions.
void afficheMenu();
void actionAFaire(int requete);
void consultation();
void action(int indice);
void recherche(int codeTaper, int cheque);
//

int main()
{
  switch(fork())
  {
    case -1: fprintf(stderr, "erreur\n");
      exit(EXIT_FAILURE);
      break;
    case 0:
      afficheMenu();
      break;
    default:
      wait(0);
  }

  return 0;
}

/* Affiche un menu principal à l'écran et lit une entrée au clavier */
void afficheMenu()
{
  int requete = 0;
  printf("\nVous etes dans le gestionnaire de comptes bancaires \n");
  while(requete != 4)
  {

    ptr_cheque = open("cpt.txt", O_RDWR);
    printf("\n****MENU****\n");
    printf("\nChoisisez une requête :\n");
    printf("(1) pour consulter le solde\n");
    printf("(2) pour créditer un solde\n");
    printf("(3) pour débiter un solde \n");
    printf("(4) pour quitter le gestionnaire \n");

    scanf("%d", &requete);
    actionAFaire(requete);

    close(ptr_cheque);
  }
}

/* Appelle la fonction correspondant à l'entrée au clavier */
void actionAFaire(int commande)
{
  if(commande == 1)
  {
    consultation();
  }
  else if(commande == 2)
  {
    action(0);
  }
  else if(commande == 3)
  {
    action(1);
  }
  else if(commande == 4)
  {
    printf("Merci à la prochaine.\n");
  }
  else
  {
    printf("commande invalide");
  }
}

/* Affiche le solde d'un compte dont le numéro est entré au clavier */
void consultation()
{
  int codeTaper, base = 10, code, solde;
  char compte [17];
  char ** fin = NULL;

  printf("Entrez le numero de compte SVP :\n");
  scanf("%d", &codeTaper);

  while(read(ptr_cheque, compte, 17))
  {
    code = strtol(compte, fin, base);
    if(codeTaper == code)
    {
      lseek(ptr_cheque, -5, SEEK_CUR);
      read(ptr_cheque, compte, 4);
      solde = strtol(compte, fin, base);
      printf("Votre solde est de : %d$\n", solde);
      break;
    }
  }
}

/* Parcours le fichier en segments de 17 caractères afin de trouver un 
 * compte dont le numéro est codeTaper et positionne le descripteur de fichier
 * à la bonne position */
void recherche(int codeTaper, int cheque)
{
  int base = 10, code;
  char ** fin = NULL;
  char compte [17];

  while(read(cheque, compte, 17))
  {
    code = strtol(compte, fin, base);
    if(codeTaper == code)
    {
      lseek(cheque, -17, SEEK_CUR);
      break;
    }
  }
}

/* Verrouille le compte entré au clavier, procède au débit ou au crédit du solde 
 * selon indice, puis déverouille le compte */
void action(int indice)
{
  int codeTaper, base = 10, solde, soldeErreur, montant;
  char compte [17];
  char ** fin = NULL;

  printf("Entrer le numero de compte :\n");
  scanf("%d", &codeTaper);

  recherche(codeTaper, ptr_cheque);

  read(ptr_cheque, compte, 17);

  if(lockf(ptr_cheque, F_TEST, 17) == -1)
  {
    printf("Le compte est verrouillé, veuillez patienter SVP\n");
  }

  if(lockf(ptr_cheque, F_LOCK, 17) >= 0)
  {
    //on ouvre de nouveau le fichier pour voir les modifications
    int ptr_cheque_fils = open("cpt.txt", O_RDWR);

    recherche(codeTaper, ptr_cheque_fils);

    read(ptr_cheque_fils, compte, 17);

    soldeErreur = solde = strtol(compte + 12, fin, base);
    printf("\nVotre solde actuel : %d$\n", solde);

    if(indice == 0)
    {
      printf("Le compte est maintenant verrouillé, entrez le montant à "
              "crediter :");
      scanf("%d", &montant);

      solde = solde + montant;

      printf("Nouveau solde :%d$\n", solde);
    }
    else
    {
      printf("Le compte est maintenant verrouillé, entrez le montant à "
              "debiter :");
      scanf("%d", &montant);

      solde = solde - montant;
        
      printf("Nouveau solde :%d$\n", solde);
    }

    if(solde < 0)
    {
      sleep(1);
      printf("\nOperation annulée le compte ne peut être dans le négatif "
              "désolé\n");
      printf("Votre solde actuel : %d$\n\n", soldeErreur);
    }
    else
    {
      sprintf(compte + 12, "%d", solde);
      lseek(ptr_cheque, -17, SEEK_CUR);
      write(ptr_cheque, compte, 16);
    }

    sleep(1);
    lockf(ptr_cheque, F_ULOCK, 17);
    close(ptr_cheque_fils);
    printf("Le compte est maintenant deverrouillé\n");
  }
  else
  {
    printf("\n Erreur \n");
  }
}

