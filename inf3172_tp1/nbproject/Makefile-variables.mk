#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-MacOSX
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-MacOSX
CND_ARTIFACT_NAME_Debug=inf3172_tp1
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-MacOSX/inf3172_tp1
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-MacOSX/package
CND_PACKAGE_NAME_Debug=inf3172tp1.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-MacOSX/package/inf3172tp1.tar
# Release configuration
CND_PLATFORM_Release=GNU-MacOSX
CND_ARTIFACT_DIR_Release=dist/Release/GNU-MacOSX
CND_ARTIFACT_NAME_Release=inf3172_tp1
CND_ARTIFACT_PATH_Release=dist/Release/GNU-MacOSX/inf3172_tp1
CND_PACKAGE_DIR_Release=dist/Release/GNU-MacOSX/package
CND_PACKAGE_NAME_Release=inf3172tp1.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-MacOSX/package/inf3172tp1.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
